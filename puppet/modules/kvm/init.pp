#
# kvm server
# http://confluence.iovation.com/pages/viewpage.action?pageId=6389858
#

class ::kvm {
  $kvm_packages = [
    "virt-top"
  ]
  package { $kvm_packages:
    ensure     => present,
  }
  file {
    "/usr/local/sbin/createvm.sh":
      content => template("kvm/createvm.sh"),
      mode    => '0755', owner => root, group => root;
    "/usr/local/sbin/genmac.sh":
      content => template("kvm/genmac.sh"),
      mode    => '0755', owner => root, group => root;
    "/usr/local/bin/kimchi_inst.sh":
      source  => "puppet:///modules/iovation/kvm/kimchi_inst.sh",
      mode    => '0755', owner => root, group => root;
    "/usr/local/bin/newvm.sh":
      source  => "puppet:///modules/iovation/kvm/newvm.sh",
      mode    => '0755', owner => root, group => root;
    "/usr/local/bin/destroyvm.sh":
      source  => "puppet:///modules/iovation/kvm/destroyvm.sh",
      mode    => '0755', owner => root, group => root;
    "/usr/local/bin/create-kvm-motd.sh":
      source  => "puppet:///modules/iovation/kvm/create-kvm-motd.sh",
      mode    => '0755', owner => root, group => root;
	}
  exec { "ln -s /var/lib/libvirt/images /vm":
    path    => ["/bin", "/usr/bin"],
    onlyif  => "test ! -e /vm -a -d /var/lib/libvirt/images/",
  }
  class { 'apache':
    default_vhost => false
  }
  network::bridge::dynamic { 'br0':
    ensure => 'up',
  }
  network::if::bridge { 'eth0':
    ensure  => 'up',
    bridge  => 'br0',
  }
  class { '::libvirt':
    qemu_vnc_listen => "0.0.0.0",
    #qemu_vnc_sasl   => true,
    qemu_vnc_sasl   => false,
    qemu_vnc_tls    => false,
    listen_tls => false,
    listen_tcp => true,
    sysconfig  => {
      'LIBVIRTD_ARGS'          => '--listen',
      'LIBVIRTD_NOFILES_LIMIT' => '4096',
    },
  }
  libvirt_pool { 'mypool' :
    ensure   => present,
    type     => 'dir',
    #activate => false,
    target   => '/vm/pool-dir',
  }
  include apache::mod::proxy_http
  apache::vhost { "$hostname":
    port        => 80,
    docroot     => '/var/www',
    proxy_dest  => "http://$hostname:8000",
  }
  cron { 'kvm-motd':
    ensure  => present,
    command => "/usr/local/bin/create-kvm-motd.sh > /dev/null 2>&1",
  }
}
