# this script installs git and pulls down the repo

gitrepo="https://bitbucket.org/laird/lab.git"

# subroutine to install programs if not found
inst () {
  command -v $1 foo >/dev/null 2>&1 || { echo >&2 "$1 not found.  Installing"; sudo yum install $1 -y; }
}

inst git
git clone $gitrepo

