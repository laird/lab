# basic node configuration

Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin" }
Package {
  allow_virtual => false,
}

node default {
  include '::ntp'
}

