#
# determine where this script lives
dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )

# subroutine to install programs if not found
inst () {
  command -v $1 foo >/dev/null 2>&1 || { echo >&2 "$1 not found.  Installing"; sudo yum install $1 -y; }
}

# install puppet if not present or version is low
puppet_ver=$(rpm -q --queryformat '%{VERSION}' puppet)
if [ $? -eq 0 ]; then
	if [ $puppet_ver == "3.7.4" ]; then 
		echo "puppet is $puppet_ver"
	else
		echo "puppet is $puppet_ver"
		echo "upgrading puppet to 3.7.4"
		sudo yum update puppet-3.7.4 -y
	fi
else
	echo "installing puppet"
    sudo $dir/tools/centos_6_x.sh
fi
if [ ! -f /usr/bin/r10k ] && [ ! -f /usr/local/bin/r10k ]; then
	echo "installing r10k"
	sudo gem install r10k --no-rdoc --no-ri
fi
if [ ! -d /etc/puppet ]; then
	echo "creating dir /etc/puppet"
	sudo mkdir /etc/puppet
fi
inst git 

sudo cp -r $dir/puppet/* /etc/puppet/.
sudo cp $dir/puppet/r10k.yaml /etc/r10k.yaml
cd /etc/puppet
echo "----------------------------"
echo "running r10k"
echo ""
r10k=`which r10k`
sudo $r10k puppetfile install
# r10k purges modules so lets replace
sudo cp -r $dir/puppet/modules/* /etc/puppet/modules/.

# add aliases to /etc/profile.d/pup.sh
echo "-----------------------------------"
echo "Adding pup shell alias"
echo ""
if [ ! -f /etc/profile.d/pup.sh ]; then
        echo "creating /etc/profile.d/pup.sh to add handy aliases"
        echo "alias pup='sudo puppet apply --pluginsync --hiera_config /etc/puppet/hiera.yaml --config=/etc/puppet/puppet.conf --modulepath=/etc/puppet/modules /etc/puppet/manifests'" > /tmp/pup.sh
        sudo mv /tmp/pup.sh /etc/profile.d/.
fi

source /etc/profile.d/pup.sh
echo "-----------------------------------"
echo "Running puppet"
echo ""
cd /etc/puppet
puppet module list --modulepath=/etc/puppet/modules
sudo puppet apply --pluginsync --hiera_config /etc/puppet/hiera.yaml --config=/etc/puppet/puppet.conf --modulepath=/etc/puppet/modules /etc/puppet/manifests


