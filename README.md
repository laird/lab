# README #

This project provides a base puppet provisioning environment for lab systems.
In a nutshell a script is downloaded that installs git, pulls down a git repo, runs r10k to compose /etc/puppet/modules and then runs puppet.

# getting started
There are two main patterns this supports:
- Bare system - outside the firewall or otherwise standalone
- Vagrant system - standalone vagrant vm

The primary distinction between these is the provisioning method.  The Bare system will be provisoned via SSH and the Vagrant system provisioned from commands in the Vagrantfile.

# more reading
https://github.com/puppetlabs/r10k

# aliased commands
* pup  - runs puppet apply

# general patterns
```
[vagrant@localhost ~]$ pup -e "include cis::el6all"
```

# setup lab on bare machine and apply manifest
```
host=root@pdxnpalaird01"
tar czf - ../lab | ssh $host "tar xzvf -; ./lab/tools/setup.sh"

or

curl -s -L http://goo.gl/a8hNww -o dl.sh ; chmod 755 dl.sh ; ./dl.sh
./lab/tools/setup.sh

```

# setup lab on vagrant machine and apply manifest
```
vagrant up
vagrant ssh "pup"
```

