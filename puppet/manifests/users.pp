  # set root password
  user { 'root':
    ensure      => present,
    password    => '$1$/5S45jX2$PwL4e58Yf43tPXskilO86.'
  }
  # begin users
  user { 'alaird':
    ensure      => present,
    comment     => 'Alan Laird',
    password    => '$1$/5S45jX2$PwL4e58Yf43tPXskilO86.',
    uid         => 1087,
    managehome  => true,
    groups      => 'wheel',
  }
  # Give members of the "wheel" group sudo access
  sudo::conf { 'wheel':
    content  => "%wheel ALL=(ALL) NOPASSWD: ALL",
  }
